# kaniko-executer-arm64

Image to build image docker using kaniko as guided here: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
```
registry.gitlab.com/etangs/kaniko-executer-arm64 
```
An example of .gitlab-ci.yml:
```
build:
  stage: build
  image:
    name: registry.gitlab.com/etangs/kaniko-executer-arm64:latest
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG
```
